<?php

include __DIR__.'/vendor/autoload.php';

if(!is_file(__DIR__.'/config.php')){
    die("You must create the initial config.php file");
}
include __DIR__.'/config.php';

set_error_handler(\App\PhpHandlers::errorHandler(),E_ALL);
set_exception_handler(\App\PhpHandlers::exceptionHandler());