<?php

/*
 * This file is part of the Arnapou gw2logs package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App;

class Loop implements \IteratorAggregate
{
    /**
     * @var int
     */
    private $max_seconds;
    /**
     * @var int
     */
    private $wait_ms;

    public function __construct(int $max_seconds, int $wait_ms = 1000)
    {
        $this->max_seconds = $max_seconds;
        $this->wait_ms = $wait_ms;
    }

    public function iterate()
    {
        $start = microtime(true);
        while (microtime(true) - $start < $this->max_seconds) {
            yield microtime(true);
            usleep($this->wait_ms * 1000);
        }
    }

    public function getIterator()
    {
        return $this->iterate();
    }
}
