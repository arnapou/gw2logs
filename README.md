GW2logs
========

GW2 Raids personal logs hosted.

Technical informations for those interested :

* no database (file storage)
* need a cron `* * * * *  /usr/bin/php <path>/script/process.php`
* need a `config.php` file filled (look at `config.dist.php` for an example)

### Links

* [Screenshots](doc/screenshots.md)


### Changelog versions techniques

| Date       | Branche | Php | 
|------------|---------|-----|
| 13/05/2021 | master  | 8.0 |
| 20/10/2018 | v1.x    | 7.2 |
